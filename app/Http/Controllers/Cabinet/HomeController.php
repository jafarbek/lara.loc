<?php

namespace App\Http\Controllers\Cabinet;


use App\Http\Controllers\Controller;
use SoapClient;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('cabinet.home');
    }
}
