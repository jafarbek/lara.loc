<?php

namespace App\Http\Controllers\Admin;

use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Users\CreateRequest;
use App\Http\Requests\Admin\Users\UpdateRequest;
use App\UseCases\Auth\RegisterService;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    private $register;

    public function __construct(RegisterService $register)
    {
        $this->register = $register;
//        $this->middleware('can:users-manage');
    }

    public function index(Request $request)
    {
        /** @var $users User */
        $users = User::orderBy('id');
        if (!empty($value = $request->get('id'))){
            $users = $users->where('id',$value);
        }
        if (!empty($value = $request->get('name'))){
            $users = $users->where('name','like', "%$value%");
        }
        if (!empty($value = $request->get('email'))){
            $users = $users->where('email','like', "%$value%");
        }
        if (!empty($value = $request->get('status'))){
            $users = $users->where('status',$value);
        }
        if (!empty($value = $request->get('role'))){
            $users = $users->where('role',$value);
        }

        $users = $users->paginate(20);

        $statuses = [
            User::STATUS_WAIT=>'Waiting',
            User::STATUS_ACTIVE=>'Active',
        ];
        $roles = User::rolesList();
        return view('admin.users.index', compact('users','statuses','roles'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(CreateRequest $request)
    {
        $user = User::create($request->only(['name','email']) + [
            'password'=>bcrypt(\Str::random()),
            'status'=>User::STATUS_ACTIVE,
        ]);
        return redirect()->route('admin.users.show',$user);
    }

    public function show(User $user)
    {
        return view('admin.users.show',compact('user'));
    }

    public function edit(User $user)
    {
        $roles = User::rolesList();

        return view('admin.users.edit',compact('user','roles'));
    }


    public function update(UpdateRequest $request, User $user)
    {
        $user->update($request->only('name','email','role'));
        return redirect()->route('admin.users.show',compact('user'));
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('admin.users.index');
    }

    public function verify(User $user)
    {
        $this->register->verify($user->id);
        return redirect()->route('admin.users.show',$user);
    }
}
