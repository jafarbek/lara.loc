<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Region;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Users\CreateRequest;
use App\Http\Requests\Admin\Users\UpdateRequest;
use App\UseCases\Auth\RegisterService;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RegionsController extends Controller
{

    public function index()
    {
        $regions = Region::whereNull("parent_id")->orderBy('id','desc')->paginate(20);
        return view('admin.regions.index',compact('regions'));
    }

    public function create(Request $request)
    {
        $region = null;
        if ($request->get('id')) {
            $region = Region::findOrFail($request['id']);
        }
        return view('admin.regions.create',compact('region'));
    }

    public function store(Request $request, Region $region)
    {
        $this->validate($request,[
            'name'=>'required|string|max:255|unique:regions,name,NULL,id,parent_id,'.($request['region_id'] ?: 'NULL'),
            'slug'=>'required|string|max:255|unique:regions,slug,NULL,id,parent_id,'.($request['region_id'] ?: 'NULL'),
            'parent_id'=>'nullable|exists:regions,id',
        ]);
        $region = Region::create($request->only(['name','slug','parent_id']));
        return redirect()->route('admin.regions.show',compact('region'));
    }

    public function show(Region $region)
    {
        $regions = Region::whereParentId($region->id)->orderBy('name')->paginate(20);
        return view('admin.regions.show',compact('region','regions'));
    }

    public function edit(Region $region)
    {
        return view('admin.regions.edit',compact('region'));
    }


    public function update(Request $request, Region $region)
    {
        $this->validate($request,[
            'name'=>'required|string|max:255|unique:regions,name,'.$region->id.',id,parent_id,'.$region->parent_id,
            'slug'=>'required|string|max:255|unique:regions,slug,'.$region->id.',id,parent_id,'.$region->parent_id,
        ]);
        $region->update($request->only('name','slug'));
        return redirect()->route('admin.regions.show',compact('region'));
    }

    public function destroy(Region $region)
    {
        $region->delete();
        return redirect()->route('admin.regions.index');
    }

}
