<?php


namespace App\Entity;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * App\Entity\Region
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int|null $parent_id
 * @property Region $parent
 * @property Region[] $children
 * @method Builder roots()
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read int|null $children_count
 * @method static Builder|Region newModelQuery()
 * @method static Builder|Region newQuery()
 * @method static Builder|Region query()
 * @method static Builder|Region whereCreatedAt($value)
 * @method static Builder|Region whereId($value)
 * @method static Builder|Region whereName($value)
 * @method static Builder|Region whereParentId($value)
 * @method static Builder|Region whereSlug($value)
 * @method static Builder|Region whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Database\Factories\Entity\RegionFactory factory(...$parameters)
 */
class Region extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'parent_id', 'slug'];


    public function getAddress(): string
    {
        return ($this->parent ? $this->parent->getAddress().', ':'').$this->name;
    }

    public function parent()
    {
        return $this->belongsTo(static::class,'parent_id','id');
    }

    public function children()
    {
        return $this->hasMany(static::class,'parent_id','id');
    }


    public function scopeRoots(Builder $query)
    {
        return $query->where('parent_id', null);
    }
}
