<?php


namespace App\UseCases\Auth;


use App\Entity\User\User;
use App\Http\Requests\Auth\RegisterRequest;
use App\Mail\Auth\VerifyEmail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Mail\Mailer;
use Str;

class RegisterService
{
    private $mailer;
    private $dispatcher;

    public function __construct(Mailer $mailer, Dispatcher $dispatcher)
    {
        $this->mailer = $mailer;
        $this->dispatcher = $dispatcher;
    }

    public function register(RegisterRequest $request)
    {
        $user = User::register(
            $request['name'],
            $request['email'],
            $request['password'],
       );
        $this->mailer->to($user->email)->send(new VerifyEmail($user));
        $this->dispatcher->dispatch(new Registered($user));
    }

    public function verify($id) : void
    {
        /** @var $user User */
        $user = User::findOrFail($id);
        $user->verify();
    }
}
