<?php

namespace App\Console\Commands\User;

use App\Entity\User\User;
use App\UseCases\Auth\RegisterService;
use Illuminate\Console\Command;

class VerifyCommand extends Command
{
    /**
     * {email?}. qilib berilsa email berish shart emas bo'ladi
     *
     * @var string
     */
    protected $signature = 'user:verify {email}';
    protected $description = 'Command description';
    /**
     * @var RegisterService
     */
    private $service;
    public function __construct(RegisterService $service)
    {
        $this->service = $service;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
            /** @var $user User */
        $email = $this->argument('email');
        if (!$user = User::where('email',$email)->first()) {
            $this->error('Undifined Email');
            return false;
        }

        try {
            $this->service->verify($user->id);
        }catch (\DomainException $e){
            $this->error($e->getMessage());
            return false;
        }
        $this->info('User successfully verified');
    }
}
