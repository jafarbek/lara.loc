<?php

namespace Database\Factories\Entity\Adverts;

use App\Entity\Adverts\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $name = $this->faker->unique()->city,
            'slug' => Str::kebab($name),
            'parent_id' => null,
        ];
    }
}
