<?php

namespace Database\Seeders;


use App\Entity\Region;
use App\Entity\User\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        User::factory(1000)->create();
//        Region::factory(10)->create()->each(function (Region $region){
//            $region->children()->saveMany(Region::factory(random_int(3,10))->create()->each(function (Region $region){
//                $region->children()->saveMany(Region::factory(random_int(3,10))->make());
//            }));
//        });
        for ($i=0; $i<1000; $i++){
            $faker = \Illuminate\Container\Container::getInstance()->make(\Faker\Generator::class);
            $active = $faker->boolean;
            $phoneActive = $faker->boolean;
            $data[] = [
                'name' => $faker->name,
                'last_name' => $faker->lastName,
                'email' => $faker->unique()->safeEmail,
                'phone' => $faker->unique()->phoneNumber,
                'phone_verified' => $phoneActive,
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
                'verify_token' => $active ? null : Str::uuid(),
                'phone_verify_token' => $phoneActive ? null : Str::uuid(),
                'phone_verify_token_expire' => $phoneActive ? null : Carbon::now()->addSeconds(300),
                'role' => $active ? $faker->randomElement(User::rolesList()) : User::ROLE_USER,
                'status' => $active ? User::STATUS_ACTIVE : User::STATUS_WAIT,
            ];
        }
        User::insert($data);
    }
}
