@extends('layouts.app')

@section('content')
    @include('admin.adverts.categories._nav')

    <form method="POST" action="{{ route('admin.adverts.categories.update', $category) }}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="name" class="col-form-label">Name</label>
            <input id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $category->name) }}" required>
            @if ($errors->has('name'))
                <span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="slug" class="col-form-label">Slug</label>
            <input id="slug" type="slug" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}" name="slug" value="{{ old('slug', $category->slug) }}" required>
            @if ($errors->has('slug'))
                <span class="invalid-feedback"><strong>{{ $errors->first('slug') }}</strong></span>
            @endif
        </div>


        <div class="form-group">
            <label for="parent" class="col-form-label">Parent</label>
            <select name="parent" type="email" id="parent" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}">
                @foreach($parents as $parent)
                    <option value="{{$parent->id==$category->parent_id ? $category->parent_id : $parent->id}}" {{$parent->id==$category->parent_id ? 'selected' : ''}}>
                        @for ($i = 0; $i < $parent->depth; $i++) &mdash; @endfor
                        {{$parent->id==$category->parent_id ? $category->parent->name : $parent->name}}
                    </option>
                @endforeach
            </select>
            @if ($errors->has('parent'))
                <span class="invalid-feedback"><strong>{{ $errors->first('slug') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection
