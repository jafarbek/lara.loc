@extends('layouts.app')
@section('content')
    @include('admin.regions._nav')

    <div class="d-flex flex-row mb-3">
        <a href="{{ route('admin.regions.edit', $region) }}" class="btn btn-primary mr-1">Edit</a>
        <form method="POST" action="{{ route('admin.regions.destroy', $region) }}" class="mr-1">
            @csrf
            @method('DELETE')
            @can('regions-manage')
            <button class="btn btn-danger">Delete</button>
            @endcan
        </form>
    </div>

    <table class="table table-bordered table-striped">
        <tbody>
        <tr>
            <th>ID</th><td>{{ $region->id }}</td>
        </tr>
        <tr>
            <th>Name</th><td>{{ $region->name }}</td>
        </tr>
        <tr>
            <th>Email</th><td>{{ $region->slug }}</td>
        </tr>
        </tbody>
    </table>
    <p><a href="{{ route('admin.regions.create', ['id'=>$region->id]) }}" class="btn btn-success">Add SubRegion</a></p>

    @include('admin.regions._list')

    {{ $regions->links() }}
@endsection
