<?php

use Illuminate\Support\Facades\Route;


Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/login/phone', [\App\Http\Controllers\Auth\LoginController::class,'phone'])->name('login.phone');
Route::post('/login/phone', [\App\Http\Controllers\Auth\LoginController::class,'verify']);

Route::get('/verify/{token}', [\App\Http\Controllers\Auth\RegisterController::class, 'verify'])->name('register.verify');


Route::get('/cabinet', [\App\Http\Controllers\Cabinet\HomeController::class,'index'])->name('cabinet.home');

// Cabinet of User
Route::group(
    [
        'prefix' => 'cabinet',
        'as' => 'cabinet.',
        'namespace' => 'App\Http\Controllers\Cabinet',
        'middleware' => ['auth'],
    ],
    function(){
        Route::get('/',[\App\Http\Controllers\Cabinet\HomeController::class,'index'])->name('home');
        Route::group(['prefix' => 'profile','as'=>'profile.'],function (){
            Route::get('/',[\App\Http\Controllers\Cabinet\ProfileController::class,'index'])->name('home');
            Route::get('/edit',[\App\Http\Controllers\Cabinet\ProfileController::class,'edit'])->name('edit');
            Route::put('/update',[\App\Http\Controllers\Cabinet\ProfileController::class,'update'])->name('update');
            Route::get('/phone',[\App\Http\Controllers\Cabinet\PhoneController::class,'form'])->name('phone');
            Route::post('/phone',[\App\Http\Controllers\Cabinet\PhoneController::class,'request']);
            Route::put('/phone',[\App\Http\Controllers\Cabinet\PhoneController::class,'verify'])->name('phone.verify');

            Route::post('/phone/auth', [\App\Http\Controllers\Cabinet\PhoneController::class,'auth'])->name('phone.auth');
        });
        Route::resource('adverts',"Adverts\AdvertController");

    }
);
/** Adminka */
Route::group(
    [
        'prefix' => 'admin',
        'as' => 'admin.',
        'namespace' => 'App\Http\Controllers\Admin',
        'middleware' => ['auth','can:admin-panel']
    ],
    function (){
        Route::get('/',[\App\Http\Controllers\Admin\HomeController::class,'index'])->name('home');
        Route::post('/users/{user}/verify',[\App\Http\Controllers\Admin\UsersController::class,'verify'])->name('users.verify');
        Route::resource('users','UsersController');
        Route::resource('regions','RegionsController');
/** Adverts (Adminka E`lonlar) */
        Route::group(
            [
                'prefix' => 'adverts',
                'as' => 'adverts.',
                'namespace' => 'Adverts',
//                'middleware' => ['auth']
            ],
            function (){
                Route::resource('categories','CategoryController');

                Route::group(['prefix' => 'categories/{category}','as'=>'categories.'], function(){
                    Route::post('/first','CategoryController@first')->name('first');
                    Route::post('/up','CategoryController@up')->name('up');
                    Route::post('/down','CategoryController@down')->name('down');
                    Route::post('/last','CategoryController@last')->name('last');

                    Route::resource('attributes','AttributeController')->except('index');
                });

            }
        );
    }
);
