<?php

namespace Tests\Feature\Auth;

use App\Entity\User\User;
use Tests\TestCase;

class LoginTest extends TestCase
{
    public function testForm(): void
    {
        $response = $this->get('/login');

        $response
            ->assertStatus(200)
            ->assertSee('Login');
    }

    public function testErrors(): void
    {
        $response = $this->post('/login', [
            'email' => '',
            'password' => '',
        ]);

        $response
            ->assertStatus(302)
            ->assertSessionHasErrors(['email', 'password']);
    }

    public function testWait(): void
    {
        $user = User::factory()->create(['status' => User::STATUS_WAIT])->first();

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'secret',
        ]);

        $response
            ->assertStatus(302)
            ->assertRedirect('/');
    }

    public function testActive(): void
    {
        $user = User::factory()->create(['status' => User::STATUS_ACTIVE])->first();


        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => '12345678',
        ]);

        $response
            ->assertStatus(302)
            ->assertRedirect('/cabinet');

        $this->assertAuthenticated();
    }
}
